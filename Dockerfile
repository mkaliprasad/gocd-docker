#
# Originally Java Dockerfile from https://github.com/dockerfile/java
#

# Pull base image
FROM library/ubuntu:14.04
MAINTAINER go-agent <manu@xola.com>

RUN apt-get update && apt-get -y upgrade && apt-get -y install software-properties-common && add-apt-repository ppa:webupd8team/java  && apt-get update
RUN (echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections) && apt-get install -y oracle-java8-installer oracle-java8-set-default
RUN apt-get install -y wget curl


# Modified to wget the agent from the download site. When a package repo
# is available we can use that and always get the lastest
RUN wget https://download.gocd.org/binaries/17.11.0-5520/deb/go-agent_17.11.0-5520_all.deb
RUN dpkg -i go-agent_17.11.0-5520_all.deb
RUN apt-get install -y apache2-utils curl

# This file is also very specific to my installation. It tells the Go agent where the Go server
# is on my internal network.
ADD go-agent /etc/default/go-agent 
ADD go-agent-1 /etc/default/go-agent-1 
ADD go-agent-2 /etc/default/go-agent-2


# Fetch PHP 5.6 repo
ADD php56.sh /
RUN bash -c "/php56.sh"
# This file has my authorization string so that I don't have to approve new agents. 
ADD autoregister.properties /var/lib/go-agent/config/autoregister.properties
ADD autoregister.properties /var/lib/go-agent-1/config/autoregister.properties
ADD autoregister.properties /var/lib/go-agent-2/config/autoregister.properties

ADD password.test.yml  /var/lib/go-agent/config/
ADD jdk.sh /etc/profile.d/

# Update software list and install php + nginx
RUN apt-get install -y acl nginx-extras git ant

# Install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install mongo3.4
RUN echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list && apt-get update
RUN apt-get install -y mongodb-org

#Install Chrome headless
RUN apt-get update &&  apt-get install -y libappindicator1 fonts-liberation libasound2 libgconf-2-4 libnspr4 libxss1 libnss3 xdg-utils
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && dpkg -i google-chrome-stable_current_amd64.deb

# This should probably be something like supervisord to keep the container running
RUN chown go.go /var/lib/go-agent-1/config && chown go.go /var/lib/go-agent-2/config
RUN mkdir -p /opt/xola/cache && mkdir -p /opt/xola/config && chown go.go /opt/xola/config && chown go.go /opt/xola/cache
RUN chown go.go /opt/xola && chmod 775 /opt/xola
RUN chmod 775 /opt/xola && chmod 775 /opt/xola/config && chmod 775 /opt/xola/cache
RUN mkdir -p /var/log/xola && chown go.go /var/log/xola && chmod 755 /var/log/xola
ADD password.prod.yml /opt/xola/config/password.prod.yml 
RUN chown go.go /opt/xola/config/password.prod.yml && chmod 777 /opt/xola/config/password.prod.yml
CMD /usr/share/go-agent/agent.sh && tail -F /var/log/go-agent/go-agent.log