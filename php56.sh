apt-get install -y software-properties-common python-software-properties aptitude
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get update && apt-get install -y nodejs phantomjs
ln -s /usr/bin/nodejs /usr/bin/node
npm install -g less@1.3.3
npm install -g handlebars@1.3
npm install -g grunt
#add-apt-repository ppa:ondrej/php -y
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install -y --allow-unauthenticated php5.6
apt-get install -y --allow-unauthenticated php5.6-cgi php5.6-curl php5.6-dom php5.6-gd php5.6-imagick php5.6-intl php5.6-mbstring php5.6-mcrypt php5.6-mongo php5-mongo php5.6-xml php5.6-zip php5.6-fpm
phpenmod mongo
phpenmod mcrypt
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
apt-get install --no-install-recommends php5.6-apcu -y --allow-unauthenticated
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6

service php5.6-fpm start

cp /etc/init.d/go-agent /etc/init.d/go-agent-1 && sed -i 's/# Provides: go-agent$/# Provides: go-agent-1/g' /etc/init.d/go-agent-1 && ln -s /usr/share/go-agent /usr/share/go-agent-1 && mkdir -p /var/{lib,log}/go-agent-1 &&  chown go:go /var/{lib,log}/go-agent-1 && update-rc.d go-agent-1 defaults

cp /etc/init.d/go-agent /etc/init.d/go-agent-2 && sed -i 's/# Provides: go-agent$/# Provides: go-agent-2/g' /etc/init.d/go-agent-2 && ln -s /usr/share/go-agent /usr/share/go-agent-2 &&  mkdir -p /var/{lib,log}/go-agent-2 &&  chown go:go /var/{lib,log}/go-agent-2 && update-rc.d go-agent-2 defaults

cp /etc/init.d/go-agent /etc/init.d/go-agent-3 && sed -i 's/# Provides: go-agent$/# Provides: go-agent-3/g' /etc/init.d/go-agent-3 && ln -s /usr/share/go-agent /usr/share/go-agent-3 &&  mkdir -p /var/{lib,log}/go-agent-3 &&  chown go:go /var/{lib,log}/go-agent-3 && update-rc.d go-agent-3 defaults